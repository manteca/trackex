# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

p "Deleting all users.."
User.destroy_all
p "Starting Seeding Users..."
User.create([{id: 1, email: 'fvillalobos@medioclick.com', password: 12345678, password_confirmation: 12345678 }])
User.create([{id: 2, email: 'mantecoso@gmail.com', password: 12345678, password_confirmation: 12345678 }])

p "---------"

p "Deleting all Companies.."
Company.destroy_all
p "Starting Seeding Companies..."
5.times do |i|
  i = i + 1
  Company.create(id: i, razon_social: "Compania ##{i}", rut: "76.134.13#{i}", representante: "Pedro #{i}", domicilio: "Bustamente 76", giro: "Restaurant #{i}", telefono: 22772907, mail: "felipe#{i}@gmail.com")
end

p "---------"

p "Deleting all Employes.."
Employee.destroy_all
p "Starting Seeding Employes..."
@x = 0;
20.times do |i|
  @x = @x + 1;
  if @x > 4
    @x = 1
  end
  i = i + 1
  Employee.create(id: i, company_id: @x, nombre: "Nombre ##{i}", rut: "76.134.13#{i}", cargo: "Cargo #{i}",  telefono: 22772907, mail: "felipe#{i}@gmail.com")
end

p "---------"

p "Deleting all Cars.."
Car.destroy_all
p "Starting Seeding Cars..."

20.times do |i|
  i = i + 1
  marcas = Array.[]('Hiunday','Toyota','Audi','BMW','Mercedes','Porsche')
  modelo = Array.[]('SUV','Camioneta','Sedan')
  ano = Array.[](2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015)
  Car.create(
    id: i, 
    employee_id: i, 
    marca: marcas.at(rand(0..5)), 
    modelo: modelo.at(rand(0..2)), 
    ano: ano.at(rand(0..9)), 
    patente: "YETR"+rand(10..99).to_s, 
    valor_tasacion: 1, 
    valor_adquisicion: 1, 
    valor_soap: 1, 
    valor_seguro_accidente: 1,
    valor_permiso_circulacion: 1,
    ano_depreciacion: 1,
    nuevo_dep_total_lineal: 1,
    nuevo_dep_anual: 1,
    nuevo_cuota_dep_mensual: 1,
    nuevo_residual: 1,
    usado_dep_total_lineas: 1,
    usado_dep_anual: 1,
    usado_cuota_dep_mensual: 1,
    usado_residual: 1)
end
