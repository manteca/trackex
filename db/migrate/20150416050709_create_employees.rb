class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :nombre
      t.string :rut
      t.string :cargo
      t.string :telefono
      t.string :mail
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
