class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :marca
      t.string :modelo
      t.integer :ano
      t.string :patente
      t.integer :valor_tasacion
      t.integer :valor_adquisicion
      t.integer :valor_soap
      t.integer :valor_seguro_accidente
      t.integer :valor_permiso_circulacion
      t.integer :ano_depreciacion
      t.integer :nuevo_dep_total_lineal
      t.integer :nuevo_dep_anual
      t.integer :nuevo_dep_anual
      t.integer :nuevo_cuota_dep_mensual
      t.integer :nuevo_residual
      t.integer :usado_dep_total_lineas
      t.integer :usado_dep_anual
      t.integer :usado_cuota_dep_mensual
      t.integer :usado_residual
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
