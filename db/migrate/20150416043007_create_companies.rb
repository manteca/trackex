class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :razon_social
      t.string :rut
      t.string :representante
      t.string :domicilio
      t.string :giro
      t.string :telefono
      t.string :mail

      t.timestamps null: false
    end
  end
end
