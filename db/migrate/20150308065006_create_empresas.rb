class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :razon_social
      t.string :rut
      t.string :representante
      t.string :giro
      t.string :doreccion

      t.timestamps null: false
    end
  end
end
