class CompaniesController < ApplicationController

  active_scaffold :"company" do |conf|
    conf.label = "Compañia"
    #conf.list.columns = [:rut, :razon_social, :giro, :representante, :domicilio, :telefono, :mail]
    conf.nested.add_link(:employees)
    #conf.list.sorting = { :title => :desc }
  end
end
