class EmployeesController < ApplicationController
  active_scaffold :"employee" do |conf|
    conf.label = "Empleados"
    #conf.list.columns = [:rut, :nombre, :cargo, :mail, :telefono, :company, :cars]
    conf.nested.add_link(:cars)
  end
end
