json.array!(@users) do |user|
  json.extract! user, :id, :razon_social, :rut, :representante, :giro, :doreccion
  json.url user_url(user, format: :json)
end
