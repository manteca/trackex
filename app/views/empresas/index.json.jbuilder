json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :razon_social, :rut, :representante, :giro, :doreccion
  json.url empresa_url(empresa, format: :json)
end
