require 'test_helper'

class CarsControllerTest < ActionController::TestCase
  setup do
    @car = cars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create car" do
    assert_difference('Car.count') do
      post :create, car: { ano: @car.ano, ano_depreciacion: @car.ano_depreciacion, employee_id: @car.employee_id, marca: @car.marca, modelo: @car.modelo, nuevo_cuota_dep_mensual: @car.nuevo_cuota_dep_mensual, nuevo_dep_anual: @car.nuevo_dep_anual, nuevo_dep_anual: @car.nuevo_dep_anual, nuevo_dep_total_lineal: @car.nuevo_dep_total_lineal, nuevo_residual: @car.nuevo_residual, patente: @car.patente, usado_cuota_dep_mensual: @car.usado_cuota_dep_mensual, usado_dep_anual: @car.usado_dep_anual, usado_dep_total_lineas: @car.usado_dep_total_lineas, usado_residual: @car.usado_residual, valor_adquisicion: @car.valor_adquisicion, valor_permiso_circulacion: @car.valor_permiso_circulacion, valor_seguro_accidente: @car.valor_seguro_accidente, valor_soap: @car.valor_soap, valor_tasacion: @car.valor_tasacion }
    end

    assert_redirected_to car_path(assigns(:car))
  end

  test "should show car" do
    get :show, id: @car
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @car
    assert_response :success
  end

  test "should update car" do
    patch :update, id: @car, car: { ano: @car.ano, ano_depreciacion: @car.ano_depreciacion, employee_id: @car.employee_id, marca: @car.marca, modelo: @car.modelo, nuevo_cuota_dep_mensual: @car.nuevo_cuota_dep_mensual, nuevo_dep_anual: @car.nuevo_dep_anual, nuevo_dep_anual: @car.nuevo_dep_anual, nuevo_dep_total_lineal: @car.nuevo_dep_total_lineal, nuevo_residual: @car.nuevo_residual, patente: @car.patente, usado_cuota_dep_mensual: @car.usado_cuota_dep_mensual, usado_dep_anual: @car.usado_dep_anual, usado_dep_total_lineas: @car.usado_dep_total_lineas, usado_residual: @car.usado_residual, valor_adquisicion: @car.valor_adquisicion, valor_permiso_circulacion: @car.valor_permiso_circulacion, valor_seguro_accidente: @car.valor_seguro_accidente, valor_soap: @car.valor_soap, valor_tasacion: @car.valor_tasacion }
    assert_redirected_to car_path(assigns(:car))
  end

  test "should destroy car" do
    assert_difference('Car.count', -1) do
      delete :destroy, id: @car
    end

    assert_redirected_to cars_path
  end
end
